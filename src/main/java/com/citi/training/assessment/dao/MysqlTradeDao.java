package com.citi.training.assessment.dao;

import java.sql.Connection; 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.exceptions.TradeNotFoundException;
import com.citi.training.assessment.model.Trade;

@Component
public class MysqlTradeDao implements TradeDao {

	@Autowired
	JdbcTemplate tradeTpl;

	@Override
	public Trade create(Trade trade) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		tradeTpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(
						"insert into trade (stock, price, volume) " 
								+ "values (?, ?, ?)", 
								Statement.RETURN_GENERATED_KEYS);

				ps.setString(1, trade.getStock());
				ps.setDouble(2, trade.getPrice());
				ps.setInt(3, trade.getVolume());
				return ps;

			}
		}, keyHolder);
		trade.setId(keyHolder.getKey().intValue());
		return trade;
	}

	@Override
	public List<Trade> retrieveAll() {

		return tradeTpl.query(
				"SELECT id, stock, price, volume FROM trade", 
				new TradeMapper());
	}

	@Override
	public Trade retrieveById(int id) {
		List<Trade> trades = tradeTpl.query("SELECT id, stock, price, volume FROM trade WHERE id=?", new Object[] { id },
				new TradeMapper());

		if (trades.size() <= 0) {
			throw new TradeNotFoundException("Trade id [" + id + "] not found");
		}

		return trades.get(0);
	}

	@Override
	public int deleteById(int id) {
		retrieveById(id);
		return tradeTpl.update("DELETE FROM trade WHERE id=?", id);
	}

	private static final class TradeMapper implements RowMapper<Trade> {

		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(
					rs.getInt("id"), 
					rs.getString("stock"), 
					rs.getDouble("price"), 
					rs.getInt("volume"));
		}

	}
}