package com.citi.training.assessment.dao;

import java.util.List;

import com.citi.training.assessment.model.Trade;

public interface TradeDao {

	Trade create(Trade trade);

	List<Trade> retrieveAll();

	Trade retrieveById(int id);

	int deleteById(int id);

}
