package com.citi.training.assessment.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.exceptions.TradeNotFoundException;
import com.citi.training.assessment.model.Trade;

@Component
@Profile("inmem-dao")
public class H2TradeDao implements TradeDao{
	
	private static AtomicInteger idGenerator = new AtomicInteger();

    private Map<Integer, Trade> allTrades = new HashMap<Integer, Trade>();

	@Override
	public Trade create(Trade trade) {
		trade.setId(idGenerator.addAndGet(1));
        allTrades.put(trade.getId(), trade);
        return trade;
	}

	   @Override
	    public List<Trade> retrieveAll() {
	        return new ArrayList<Trade>(allTrades.values());
	    }

	@Override
	public Trade retrieveById(int id) {
		  Trade trade = allTrades.get(id);
	        if (trade == null) {
	            throw new TradeNotFoundException("Trade has not been found");
	        }
	        return trade;
	}

	@Override
	public int deleteById(int id) {
		 Trade affectedTrades = allTrades.remove(id);
	        if (affectedTrades == null) {
	            throw new TradeNotFoundException("Trade could not be deleted");
	        }
		return id;
	}
	
	

}
