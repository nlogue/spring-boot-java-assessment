package com.citi.training.assessment.service;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;

@Component
public class TradeService {

	@Autowired
	TradeDao tradeDao;

	public Trade create(Trade trade) {
		if(trade.getStock().equals("") || 
				trade.getPrice()<=0 || 
				trade.getVolume() <=0) {
			throw new RuntimeException("Invalid Trade: Could not be created");
		}
		return tradeDao.create(trade);
	}

	public List<Trade> retrieveAll(){
		return tradeDao.retrieveAll();
	}

	public Trade retrieveById(int id) {
		return tradeDao.retrieveById(id);
	}

	public int deleteById(int id) {
		return tradeDao.deleteById(id);
	}

}
