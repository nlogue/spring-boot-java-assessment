package com.citi.training.assessment.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.assessment.model.Trade;
import com.citi.training.assessment.service.TradeService;


@RestController
@RequestMapping("/trades")
public class TradeController {

	@Autowired
	TradeService tradeService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Trade> retrieveAll() {
		return tradeService.retrieveAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Trade retrieveById(@PathVariable int id) {
		return tradeService.retrieveById(id);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Trade> create(@RequestBody Trade trade) {
		return new ResponseEntity<Trade>(tradeService.create(trade), HttpStatus.CREATED);
	}
	
	  @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	    @ResponseStatus(HttpStatus.NO_CONTENT)
	    public @ResponseBody int deleteById(@PathVariable int id) {
	        return tradeService.deleteById(id);
	    }

}
