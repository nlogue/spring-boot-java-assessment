package com.citi.training.assessment.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.assessment.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {

	@Autowired
	MysqlTradeDao mysqlTradeDao;
	//private static Logger LOG = LoggerFactory.getLogger(MysqlTradeDaoTests.class);

	@Test
	@Transactional
	public void test_createAndGetAllTrades() {
		Trade[] mockTradeArray = new Trade[8];

		for(int i=0; i<mockTradeArray.length; ++i) {
			mockTradeArray[i] = new Trade(-1, "AAPL", 179.95, 100);

			mysqlTradeDao.create(mockTradeArray[i]);
		}
	}

}
