package com.citi.training.assessment.dao;

import static org.junit.Assert.assertTrue; 

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.assessment.exceptions.TradeNotFoundException;
import com.citi.training.assessment.model.Trade;


public class H2TradeDaoTests {

    private static Logger LOG = LoggerFactory.getLogger(H2TradeDaoTests.class);

    private int testId = 879;
    private String testStock = "AAPL";
    private double testPrice = 179.64;
    private int testVolume = 10;
    

    @Test
    public void test_createAndFindTrade() {
        Trade testTrade = new Trade(-1, testStock, testPrice, testVolume);
        H2TradeDao testTradeAccess = new H2TradeDao();

        testTrade = testTradeAccess.create(testTrade);
 
        assertTrue(testTradeAccess.retrieveById(testTrade.getId()).equals(testTrade));

    }

    @Test
    public void test_createAndGetAllTrades() {
        Trade[] mockTradeArray = new Trade[8];
        H2TradeDao testTradeAccess = new H2TradeDao();

        for(int i=0; i<mockTradeArray.length; ++i) {
            mockTradeArray[i] = new Trade(-1, testStock, testPrice, testVolume);

            testTradeAccess.create(mockTradeArray[i]);
        }

        List<Trade> tradesFound = testTradeAccess.retrieveAll();
        LOG.info("Found [" + tradesFound.size() +
                 "] saved Trades");

        for(Trade trade: mockTradeArray) {
            assertTrue(tradesFound.contains(trade));
        }
        LOG.info("Matched All Trades");
    }

    @Test (expected = TradeNotFoundException.class)
    public void test_deleteTrade() {
    	Trade[] mockTradeArray = new Trade[8];
        H2TradeDao testTradeAccess = new H2TradeDao();

        for(int i=0; i<mockTradeArray.length; ++i) {
            mockTradeArray[i] = new Trade(testId + i, testStock, testPrice, testVolume);

            testTradeAccess.create(mockTradeArray[i]);
        }
        Trade testDeletedTrade = mockTradeArray[1];
    	testTradeAccess.deleteById(testDeletedTrade.getId());
    	LOG.info("Removed Trade");
    	LOG.info("Searching for Deleted Trade");
    	testTradeAccess.retrieveById(testDeletedTrade.getId());
    }
}


