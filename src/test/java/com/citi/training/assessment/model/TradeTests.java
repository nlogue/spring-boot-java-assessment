package com.citi.training.assessment.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TradeTests {

	private int testId = 53;
    private String testStock = "Science";
    private double testPrice = 179.04;
    private int testVolume = 100;
    
    @Test
    public void test_Trade_constructor() {
        Trade testTrade = new Trade(testId, testStock, testPrice, testVolume);

        assertEquals(testId, testTrade.getId());
        assertEquals(testStock, testTrade.getStock());
        assertEquals(testPrice, testTrade.getPrice(), 0.001);
        assertEquals(testVolume, testTrade.getVolume());
    }

    @Test
    public void test_Trade_toString() {
        String testString = new Trade(testId, testStock, testPrice, testVolume).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testStock));
        assertTrue(testString.contains((new Double(testPrice)).toString()));
        assertTrue(testString.contains((new Integer(testVolume)).toString()));
    }
}
